//
//  APIManager.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import Foundation
import Alamofire

class APIManager {
        
    let apiKey = "4a9baa2e7a62cdfd5d2372b2306cf666"
    let baseURL = "https://api.themoviedb.org/3"
    
    static let sharedInstance = APIManager()
    
    func getPopularMovies(pageNumber: Int, completion: @escaping (PopularMoviesResponse?, Error?) -> ()) {

        let urlStr = "\(baseURL)/movie/popular?api_key=\(apiKey)&page=\(pageNumber)"
        let url = URL.init(string: urlStr)!
        

        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                print(response.result)

                switch response.result {

                case .success(_):
                    if let json = response.value
                    {
                        print(json);
                        let resp = JSONParser.parsePopularMoviesResponse(responseObject: json)
                        completion(resp, nil)
                    }
                    break
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(nil, error)
                    break
                }
            }
        }
    
}
