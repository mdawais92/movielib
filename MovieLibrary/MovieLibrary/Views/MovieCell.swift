//
//  MovieCell.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import Foundation
import UIKit

class MovieCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
}
