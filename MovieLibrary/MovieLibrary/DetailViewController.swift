//
//  DetailViewController.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import Foundation
import UIKit
import AVKit


class DetailViewController: UIViewController {
    
    var selectedMovie: Movie!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
    let playerViewController = AVPlayerViewController()
    
    override func viewDidLoad() {
        
        self.titleLabel.text = selectedMovie.title
        
        self.posterImageView.kf.setImage(with: selectedMovie.posterUrl())
        
    }
    
    @IBAction func platVideo() {
          
//        Passing test video url, Looking how to get trailer against any movie
        
        guard let videoURL = URL(string: "https://www.youtube.com/watch?v=HXoVSbwWUIk") else {
                return
        }
        let player = AVPlayer(url: videoURL)
        
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            self.playerViewController.player?.play()
        }
        
    }
    
}
