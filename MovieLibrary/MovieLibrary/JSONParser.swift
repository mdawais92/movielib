//
//  JSONParser.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import Foundation

class JSONParser: NSObject {
    
    class func parsePopularMoviesResponse(responseObject: Any) -> (PopularMoviesResponse?) {
        
        let object = responseObject as AnyObject
        
        let page = object.value(forKey: "page") as? Int ?? 0
        let totalPages = object.value(forKey: "total_pages") as? Int ?? 0
        var movies = [Movie]()
        
        let movieObjects = object.value(forKey: "results") as? NSArray ?? []
        
        for movie in movieObjects {
            
            let id = (movie as AnyObject).value(forKey: "id") as! Int
            let title = (movie as AnyObject).value(forKey: "title") as! String
            let poster_path = (movie as AnyObject).value(forKey: "poster_path") as? String ?? ""
            
            
            let newObject = Movie.init(id: id, title: title, poster: poster_path)
            movies.append(newObject)
        }
        
        let popularMoviesResp = PopularMoviesResponse.init(movies: movies, totalPages: totalPages, currentPage: page)
        
        return popularMoviesResp
    }
    
}
