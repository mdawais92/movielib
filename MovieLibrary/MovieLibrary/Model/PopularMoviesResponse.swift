//
//  PopularMoviesResponse.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import Foundation

struct PopularMoviesResponse: Codable {
    var movies: [Movie]
    var totalPages: Int
    var currentPage: Int

    enum CodingKeys: String, CodingKey {
        case movies = "results"
        case totalPages = "total_pages"
        case currentPage = "page"
    }
}
