//
//  Movie.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import Foundation

struct Movie: Codable {

    var id: Int
    var title: String?
    var poster: String?

    init(id: Int, title: String, poster: String) {
        self.id = id
        self.title = title
        self.poster = poster
    }

    func posterUrl() -> URL? {
        return URL(string: "https://image.tmdb.org/t/p/w500/\(poster ?? "")")
    }

    enum CodingKeys: String, CodingKey {
        case id, title
        case poster = "poster_path"
    }
}
