//
//  ViewController.swift
//  MovieLibrary
//
//  Created by MAc on 27/06/2021.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var popularMoviesResponse : PopularMoviesResponse?
    var currentPage = 1;
    var totalPages = Int.max
    
    var isLoading = false
    
    var movies = [Movie]()
    
    var selectedMovie: Movie!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Add these calls in view model class in future
        
        APIManager.sharedInstance.getPopularMovies(pageNumber: 1) { [weak self]
            response, error in
            
            if (error == nil) {
                
                self!.popularMoviesResponse = response
                self!.currentPage = response!.currentPage
                self!.totalPages = response!.totalPages
                
                self!.movies = response?.movies ?? []
                
                self!.tableView.reloadData()
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toDetail" {
            
            let destnation = segue.destination as! DetailViewController
            destnation.selectedMovie = self.selectedMovie
        }
        
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell") as? MovieCell else { return UITableViewCell() }
        
        let movie = movies[indexPath.row]
        
        cell.titleLabel.text = movie.title
        
        cell.posterImageView.kf.setImage(with: movie.posterUrl())
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedMovie = movies[indexPath.row]
            
        self.performSegue(withIdentifier: "toDetail", sender: self)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height) && isLoading == false {
            
            // Move to view model in future
            
            if currentPage > totalPages {
                return
            }
                
            isLoading = true
            
            print("Load new")
            
            APIManager.sharedInstance.getPopularMovies(pageNumber: currentPage+1) { response, error in
                
                self.isLoading = false
                
                if (error == nil) {
                    
                    self.popularMoviesResponse = response
                    self.currentPage = response!.currentPage
                    self.totalPages = response!.totalPages
                    
                    self.movies.append(contentsOf: response!.movies)
                 
                    self.tableView.reloadData()
                }
                
            }
            
        }
        
        
        
    }
    
    
}

